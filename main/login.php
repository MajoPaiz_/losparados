<?php

require("../lib/page.php");


//*AQUI COMIENZA EL LOGIN DE LOS CLIENTES SEGUN SU USUARIO Y CONTRASEÑA//*
//* VALINADO SI EXISTE ALGO CLIENTE SI NO LO MANDARA A EL FORMALARIO DE REGISTARSE //*
$sql = "SELECT * FROM clientes";
$data = Database::getRows($sql, null);
if($data == null)
{
    header("location: register.php");
}

Page::header("Iniciar sesión");
if(!empty($_POST))
{
	$_POST = validator::validateForm($_POST);
  	$alias = $_POST['alias'];
  	$clave = $_POST['clave'];
  	try
    {
      	if($alias != "" && $clave != "")
  		{ /* selecciona todo del cliente para poder pedir los datos */
  			$sql = "SELECT * FROM clientes WHERE alias = ?";
		    $params = array($alias);
				$data = Database::getRow($sql, $params);
				
				$sqlban = "SELECT * FROM baneados WHERE codigo_cliente = ?";
				$paramsban = array($data['codigo_cliente']);
				$databan = Database::getRow($sqlban, $paramsban);
				if($databan!=null)
				{
					throw new Exception("Usted esta baneado");
				}
		    if($data != null)
		    {	/*valida los datos de la contraseña  que pertenesca a el dicho alias o usuario */

		    	$hash = $data['clave'];
		    	if(password_verify($clave, $hash)) 
		    	{ 
					
						
		        ini_set("date.timezone","America/El_Salvador");
			  		$horis= date("G:i");
		  			$fecha = date('Y-m-d');
		 		
	  				$sqlint = "INSERT INTO ingreso(codigo_cliente,hora_ingreso,fecha_ingreso) VALUES(?,?,?)";
   					$paramsint = array($data['codigo_cliente'],$horis,$fecha);
						 Database::executeRow($sqlint,$paramsint); 
						$sql5 = "UPDATE intentos SET imp = ? WHERE codigo_cliente = ?";
						$params5 = array(0,$data['codigo_cliente']);
						Database::executeRow($sql5, $params5); 

					$_SESSION['codigo_cliente'] = $data['codigo_cliente'];
					
			      	$_SESSION['nombres_cliente'] = $data['alias'];
			      	header("location: ../public/index.php");
				}
				else 
        {
          $jsql = "SELECT * FROM intentos WHERE codigo_cliente=? AND imp > 0 ";
          $jparams = array($data['codigo_cliente']);
          $jdata = Database::getRow($jsql, $jparams);
          if($jdata==null)
          {
            $cantidad=1;
            $importancia=1;
            $fecha = date('Y-m-d');
            $sqlint = "INSERT INTO intentos(codigo_cliente,cantidad,fecha,imp) VALUES(?,?,?,?)";
            $paramsint = array($data['codigo_cliente'],$cantidad,$fecha,$importancia);
            Database::executeRow($sqlint, $paramsint);
          }
          else
          {
           $cantidad=$jdata['cantidad'];
           $cantidad=$cantidad+1;    
           $sqlup = "UPDATE intentos SET cantidad = ? WHERE codigo_cliente = ? AND imp > 0 ";
           $paramsup = array($cantidad,$data['codigo_cliente']);
           Database::executeRow($sqlup, $paramsup);   

           if($cantidad>=5)
           {

            $horis= date("G:i");
            $fecha = date('Y-m-d');
            $nuevafecha = strtotime ( '+1 day' , strtotime ( $fecha ) ) ;
            $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
            $tipo=1;

            $sqlint = "INSERT INTO baneados(codigo_cliente,hora_inicio,hora_fin,fecha_inicio,fecha_fin) VALUES(?,?,?,?,?)";
            $paramsint = array($data['codigo_cliente'],$horis,$horis,$fecha,$nuevafecha);
            Database::executeRow($sqlint, $paramsint);

            throw new Exception("Ha Intentado Ingresar 5 Veces al Sistema, Sera Baneado");     
          } 


        }
        throw new Exception("La clave ingresada es incorrecta");
      }
		    }
		    else
		    {
		    	throw new Exception("El alias ingresado no existe");
		    }
	  	}
	  	else
	  	{
	    	throw new Exception("Debe ingresar un alias y una clave");
	  	}
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
	
//*AQUI SE INTRODUCE TODO LO QUE SE VERIA SEGUN EL USUARIO(SU INFORMACION)//*
}
?>
<!--parallax-->
<div class="container">
  <div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
      <div class="container ">
        <br><br>
        <h1 class="header center"><img src="../img/logo.png" width="300" height="250"></h1>
        <div class="row center">
          <h3 class="header col s12  verdeeee">Iniciar session</h3>
        </div>
        <br><br>

      </div>
    </div>
 <div class="parallax"><img src="../img/paralax2.jpg" alt="Unsplashed background img 1"></div>
  </div>

<form method='post'>
	<div class='row'>
		<div class='input-field col s12 m6 offset-m3'>
			<i class='material-icons prefix'>person_pin</i>
			<input id='alias' type='text' name='alias' class='validate' required/>
	    	<label for='alias'>Usuario</label>
		</div>
		<div class='input-field col s12 m6 offset-m3'>
			<i class='material-icons prefix'>security</i>
			<input id='clave' type='password' name='clave' class="validate" required/>
			<label for='clave'>Contraseña</label>
		</div>
	</div>
	<div class='row center-align'>
		<button type='submit' class='btn waves-effect'><i class='material-icons'>send</i></button>
	</div>
	
</form>
<div class='row center-align'>
  
<a class="waves-effect waves-light btn" href='register.php'>Registrate</a>
  	</div>
</div>
<?php
Page::footer();
?>