<?php
require("../lib/page.php");
Page::header("Eventos");
	/*inicio de formulario evento*/
 
// if de tiempo 
if (!isset($_SESSION['tiempo'])) {
    $_SESSION['tiempo']=time();
}
else if (time() - $_SESSION['tiempo'] > 600) {
    session_destroy();

 Page::showMessage(3, "amigo  se tardo en entrar a la pagina otra vez ", "../main/login.php");
    die(); 
    }
	//es para compartir los seleccionado 
if(!empty($_POST))
{
	$search = trim($_POST['buscar']);
	$sql = "SELECT * FROM eventos, clientes, sucursales WHERE eventos.codigo_cliente = clientes.codigo_cliente AND codigo_cliente=? AND eventos.codigo_sucursal = sucursales.codigo_sucursal AND alias LIKE ? ORDER BY alias";
	$params = array($_SESSION['codigo_cliente'],"%$search%");
}
else
{
	$codigito=$_SESSION['codigo_cliente'];
	$sql = "SELECT * FROM eventos, clientes, sucursales WHERE eventos.codigo_cliente = clientes.codigo_cliente AND clientes.codigo_cliente=? AND eventos.codigo_sucursal = sucursales.codigo_sucursal  ORDER BY alias";
	$params = array($codigito);
}
$data = Database::getRows($sql, $params);
if($data != null)
{
?>
<div class="container">
<!-- para poner toda el data y con eso se muestra la tabla -->
<div class="container">
<form method='post'>
	<div class='row'>
		<div class='input-field col s12 m4'>
			<a href='../public/Eventos.php' class='btn waves-effect indigo'><i class='material-icons'>add_circle</i></a>
		</div>
	</div>
</form>
<table class='striped'>
	<thead>
		<tr>
			<th>CLIENTE</th>
			<th>FECHA</th>
			<th>DIRECCIÓN</th>
			<th>SUCURSAL</th>
			
			<th>ingresos</th>
			<th>baneos</th>
			<th>ESTADO</th>
			<th>ACCIÓN</th>
		</tr>
	</thead>
	<tbody>
</div>
<?php
	foreach($data as $row)
	{
		print("
			<tr>
				<td>".$row['nombres_cliente']."</td>
				<td>".$row['fecha_evento']."</td>
				<td>".$row['direccion_evento']."</td>
				<td>".$row['nombre_sucursal']."</td>
				<td><a href='../public/reportes/reporte_ingreso.php?id=".$row['codigo_cliente']."' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a></td>
				<td><a href='../public/reportes/reporte_baneado.php?id=".$row['codigo_cliente']."'class='btn waves-effect grey'><i class='material-icons'>cancel</i></a></td>
			
				<td>
		");
		if($row['estado_evento'] == 1)
		{
			print("<i class='material-icons'>visibility</i>");
		}
		else
		{
			print("<i class='material-icons'>visibility_off</i>");
		}
		print("
				</td>
				<td>
					<a href='../public/Eventos.php?id=".$row['codigo_evento']."' class='blue-text'><i class='material-icons'>mode_edit</i></a>
					<a href='deleteeven.php?id=".$row['codigo_evento']."' class='red-text'><i class='material-icons'>delete</i></a>
				   
					</td>
			</tr>
		");
	}
	print("
		</tbody>
	</table>
	</div>
	");
} //Fin de if que comprueba la existencia de registros.
else
{
	Page::showMessage(4, "No hay registros disponibles", "../public/Eventos.php");
}
Page::footer();
?>