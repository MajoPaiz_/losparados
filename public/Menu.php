<?php
require("../lib/page.php");
Page::header("Menu");?>
<div class="container">
<!-- archivo maestro que incuye la pagina social -->
<?php include("../inc/social.php");?>


	<div class='container' id='productos'>
		<h4 class='center-align'>NUESTRO MENU</h4>
		<div class='row'>
		<?php
		
		$sql = "SELECT * FROM menu, tipo_menu WHERE menu.codigo_tipomenu = tipo_menu.codigo_tipomenu AND estado_menu = 1";
		$data = Database::getRows($sql, null);
		if($data != null)
		{
			foreach ($data as $row) 
			{
				print("
			
			
				<div class='card hoverable col s12 m6 l4'>
						<div class='card-image waves-effect waves-block waves-light'>
							<img class='activator' src='data:image/*;base64,$row[imagen_menu] '>
						</div>
						<div class='card-content'>
							<span class='card-title activator grey-text text-darken-4'>$row[nombre_menu]<i class='material-icons right'>more_vert</i></span>
						</div>
						<div class='card-reveal'>
							<span class='card-title grey-text text-darken-4'>$row[nombre_menu]<i class='material-icons right'>close</i></span>
							<p>$row[descripcion_menu]</p>
							<p>Precio (US$) $row[precio]</p>
						</div>
					</div>
				");
			}
		}
		else
		{
			print("<div class='card-panel yellow'><i class='material-icons left'>warning</i>No hay registros disponibles en este momento.</div>");
		}
		?>
		</div><!-- Fin de row -->
	</div><!-- Fin de container -->
</div>
<?php
Page::footer();
?>