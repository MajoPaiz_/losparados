<?php
require("../lib/page.php");
Page::header("Bienvenido");?>
<div class="container">
<!--parallax-->
  <div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
      <div class="container ">
        <br><br>
        <h1 class="header center"><img src="../img/logo.png" width="300" height="250"></h1>
        <div class="row center">
          <h3 class="header col s12  verdeeee">La mejor taqueria desde 1994</h3>
        </div>
        <br><br>

      </div>
    </div>
 <div class="parallax"><img src="../img/paralax2.jpg" alt="Unsplashed background img 1"></div>
  </div>
  <?php include("../inc/social.php");?>


  <div class="container">
    <div class="section">

      <!--   seccion  de comentario donde salen los comentarios de la empresa  -->
      <div class="row">
        <div class="col s12 m4">
          <div class="icon-block">
             <h5 class="center brown-text">Vision</h5>
             <p class="light">en poco tiempo ganarnos el cariño de nuestros clientes</p>
             </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
        
            <h5 class="center brown-text">¿Quienes somos?</h5>
            <p class="light">Somos un restaurante de El Salvador con comida mexicana que servimos Tacos, Tortas y Burritos. </p>
        
          </div>
        </div>

        <div class="col s12 m4">
          <div class="icon-block">
            
            <h5 class="center brown-text">Mision</h5>

            <p class="light">Ser una gran taqueria con el mejor sabor mexicano.</p>
          </div>
        </div>
      </div>

    </div>
  </div>

<div class="slider">
    <ul class="slides">
      <li>
        <img src="../img/sliderg2.jpg"> <!--imagen del slider-->
     </li>
      <li>
       <img src="../img/sliderg1.jpg"> <!--imagen del slider--> 
     </li>
     <li>
        <img src="../img/sliderg3.jpg"> <!--imagen del slider-->
     </li>
     <li>
        <img src="../img/sliderg4.jpg"> <!--imagen del slider-->
     </li>
    </ul>
  </div>
  <!--INFORMACION SEGUN LA NECESIDAD DE LA EMPRESA-->
  <div class="container">
    <div class="section">

      <div class="row">
        <div class="col s12 center">
          <h3><i class="mdi-content-send brown-text"></i></h3>
          <h4>Productos</h4>
          <p class="left-align light">Tenemos los mejores productos con los ingredientes mas frescos para poderte servir las mejores tortas y tacos de todo el pais,
      si quiere probar el mejor sabor mexicano buscanos en nuestras diferentes sucursales.
         </p>
         <a class="waves-effect waves-light btn center green" href="../main/login.php"> Más informacion</a>
        </div>
      </div>

    </div>
  </div>

  <div class="container">
    <div class="section">

      <div class="row">
        <div class="col s12 center">
          <h3><i class="mdi-content-send brown-text"></i></h3>
          <h4>Eventos</h4>
          <h7 class="center-align light">Quieres crear un evento con nosotros?</h7>
           <p class="center-align light">llena el siguiente formulario y nos pondremos en contacto para brindarte mas informacion al respecto 
         </p>
         <a class="waves-effect waves-light btn center green" href="../main/login.php"> Más informacion</a>
        </div>
      </div>

    </div>
  </div>
</div>
<?php
Page::footer();
?>