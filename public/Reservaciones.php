<?php
require("../lib/page.php");
Page::header("Reservaciones");
?>
<div class="container">
<?php include("../inc/social.php");?>

<!-- se capturan los datos de la reservacion -->

<?php
if(empty($_GET['id'])) 
{
    
    $id = null;
    $cliente = null;
    $fecha = null;
    $hora = null;
    $estado = null;
    $sucursal = null;
}
else
{
   
   $id = $_GET['id'];
    $sql = "SELECT * FROM reservaciones WHERE codigo_reservacion = ?";
    $params = array($id);
    $data = Database::getRow($sql, $params);
    $cliente = $data['codigo_cliente'];
    $fecha = $data['fecha_reservacion'];
    $hora = $data['hora_reservacion'];
    $estado = $data['estado_reservacion'];
    $sucursal = $data['codigo_sucursal'];
}
// se establece las variable para guardar en a base de datos 
if(!empty($_POST))
{
    $_POST = Validator::validateForm($_POST);
    
  	$cliente = $_POST['cliente'];
  	$fecha = $_POST['fecha'];
    $hora = $_POST['hora'];
    $estado = $_POST['estado'];
    $sucursal = $_POST['sucursal'];

    try 
    {
        if($cliente != "")
        {
            if($fecha != "")
            {
                    if($hora != "")
                    {
                        if($sucursal != "")
                        {
                            if($id == null)
                            {

                                
                                $sql = "SELECT * FROM reservaciones WHERE fecha_reservacion BETWEEN ? AND ?";
                                $params = array($fechaini, $fechafin);
                            


                                $sql = "INSERT INTO reservaciones (codigo_cliente, fecha_reservacion, hora_reservacion, estado_reservacion,codigo_sucursal) VALUES(?, ?, ?, ?, ?)";
                                $params = array($cliente, $fecha, $hora, $estado, $sucursal);
                            }
                            else
                            {
                                $sql = "UPDATE reservaciones SET codigo_cliente = ?, fecha_reservacion = ?, hora_reservacion = ?, estado_reservacion = ?,codigo_sucursal = ? WHERE codigo_reservacion = ?";
                                $params = array($cliente, $fecha, $hora, $estado, $sucursal, $id);
                            }
                            if(Database::executeRow($sql, $params))
                            {
                                Page::showMessage(1, "Reservacion completada ", "../main/reservaciones.php");
                            }
                            else
                            {
                                throw new Exception("Operación fallida");
                            }
                        }
                        else
                        {
                            throw new Exception("Debe seleccionar una sucursal");
                        }
                    }
                    else
                    {
                        throw new Exception("Debe digitar una hora");
                    }
                }
                else
                {
                    throw new Exception("Debe ingresar una fecha");
                }
            }
        else
        {
            throw new Exception("Debe seleccionar un cliente");
        }
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
?><div class="slider">
    <ul class="slides">
      <li>
        <img src="../img/sliderg2.jpg"> <!--imagen del slider-->
     </li>
      <li>
       <img src="../img/sliderg1.jpg"> <!--imagen del slider--> 
     </li>
     <li>
        <img src="../img/sliderg3.jpg"> <!--imagen del slider-->
     </li>
     <li>
        <img src="../img/sliderg4.jpg"> <!--imagen del slider-->
     </li>
    </ul>
  </div>
  <div class="container">
<form method='post' enctype='multipart/form-data'>

<div class='row'>
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>person</i>
              <?php
                  $sql = "SELECT * FROM clientes WHERE codigo_cliente = ?";
                 $params = array($_SESSION['codigo_cliente']);
                 $data = Database::getRow($sql, $params);
                 $nombres = $data['nombres_cliente'];
                 $cliente = $data['codigo_cliente'];
                 

              ?>
 	        <input id='cliente' type='text' name='cliente' class='validate hide' value='<?php print($cliente); ?>' required readonly/>
          
          	<input id='nombres_cliente' type='text' name='nombres_cliente' class='validate' value='<?php print($nombres); ?>' required readonly/>
          	<label for='nombres_cliente'>Nombres</label>
        </div>
     <!-- se crea el formulario -->
     </div>
     <div class='row'>
         <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>work</i>
            <?php
            $sql = "SELECT codigo_sucursal, nombre_sucursal FROM sucursales";
            Page::setCombo("Sucursal", "sucursal", $sucursal, $sql);
            ?>
        </div>
        
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>tab_unselected</i>
          	<input id="hora" type="time" name="hora" step="1" class='validate' value='<?php print($hora); ?>' required/>
        </div>
        
        <div class='input-field col s12 m6'>
          	<i class='material-icons prefix'>tab</i>
          	<input id="fecha" type="date" name="fecha" step="1" class='validate' value='<?php print($fecha); ?>' required/>
        </div>
        <div class='input-field col s12 m6'>
            <span>Estado:</span>
            <input id='activo' type='radio' name='estado' class='with-gap' value='1' <?php print(($estado == 1)?"checked":""); ?>/>
            <label for='activo'><i class='material-icons left'>visibility</i>Activo</label>
            <input id='inactivo' type='radio' name='estado' class='with-gap' value='0' <?php print(($estado == 0)?"checked":""); ?>/>
            <label for='inactivo'><i class='material-icons left'>visibility_off</i>Inactivo</label>
        </div>
    </div>
    <div class='row center-align'>
        <a href='index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
    </div>
    </div>
</form>
</div>
<?php
Page::footer();
?>
