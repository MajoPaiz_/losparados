<?php
require("../lib/page.php");
Page::header("sucursales");?>
<div class="container">
 <!-- Sección de productos -->

	<div class='container' id='productos'>
		<h4 class='center-align'>NUESTRO sucursales</h4>
		<div class='row'>
		<?php
	
		$sql = "SELECT * FROM sucursales";
		$data = Database::getRows($sql, null);
		if($data != null)
		{
			//se crea la trjeta con el contenido de la sucursal
			foreach ($data as $row) 
			{
				print("
					<div class='card hoverable col s12 m6 l4'>
						<div class='card-image waves-effect waves-block waves-light'>
							<img class='activator' src='data:image/*;base64,$row[imagen_sucursal] '>
						</div>
						<div class='card-content'>
							<span class='card-title activator grey-text text-darken-4'>$row[nombre_sucursal]<i class='material-icons right'>more_vert</i></span>
						</div>
						<div class='card-reveal'>
							<span class='card-title grey-text text-darken-4'>$row[nombre_sucursal]<i class='material-icons right'>close</i></span>
							<p>$row[direccion]</p>
						</div>
					</div>
				");
			}
		}
		else
		{
			print("<div class='card-panel yellow'><i class='material-icons left'>warning</i>No hay registros disponibles en este momento.</div>");
		}
		?>
		</div><!-- Fin de row -->
	</div><!-- Fin de container -->
</div>
<?php
Page::footer();
?>