<?php
//archivo aestro que incluye el page 
require("../lib/page.php");
Page::header("Participa y gana");



if(!empty($_POST))
{
    $_POST = Validator::validateForm($_POST);
  	$comentario = $_POST['textarea1'];
    $fecha = date('Y-m-d');
    $user = $_SESSION['codigo_cliente'];

    try 
    {

          if($comentario!=null)
          {
             
              $sql = "INSERT INTO comentarios(comentario, fecha_comentario, codigo_cliente) VALUES(?, ?, ?)";
              $params = array($comentario, $fecha, $user);

              if(Database::executeRow($sql, $params))
                {
                    Page::showMessage(1, "Operación satisfactoria", "index.php");
                }
                else
                {
                    throw new Exception("Operación fallida");
                }
          }
          else
          {
              throw new Exception("Ingrese un comntario");
          }
      
      
  


    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}

?> 
<div class="container">

<div class="row">
    <form method='post' class="col s12">
      <div class="row">
        <div class="input-field col s12">
          <textarea id="textarea1" required name="textarea1"class="materialize-textarea"></textarea>
          <label for="textarea1">Comentarios</label>

        </div>
      </div>
      <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
  </button>
    </form>
  </div>

</div>

<?php
Page::footer();
?>