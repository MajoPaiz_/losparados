<?php
require("../lib/page.php");
Page::header("Nosotros");?>
<div class="container">
<!-- archivo que incluye la pagina social 
<?php include("../inc/social.php");?>
<!--parallax-->
  <div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
      <div class="container ">
        <br><br>
          <div class="row center">
          <h1 class="center col s12 m12 green darken-3">Historia</h1>
        </div>
        <br><br>

      </div>
    </div>
 <div class="parallax"><img src="../img/paralax2.jpg" alt="Unsplashed background img 1"></div>
  </div>
  <div class="container">
<div class="card-panel valign-wrapper col s12 m6">
      <span class="teal-text text-darken-4  valign">Fundada en 1965 TAQUERÍA LOS PARADOS ha sido pionera en servir los mejores tacos al pastor, que por casi 50 años nos avalan.

En ese entonces sin mesas ni sillas, cautivamos a nuestros clientes hasta llegar a lo que hoy somos, una gran cadena de restaurantes mexicanos en donde siempre encontrarás la mejor calidad de productos y servicios, buscando la satisfacción de nuestros consumidores en un precio accesible y con el mejor servicio. 

Vendido más de un millón de tacos al año, TAQUERÍA LOS PARADOS tienen la filosofía de tener los mejores estándares de calidad para conservar la confianza que ya generamos y crecer nuestro mercado.

En TAQUERÍA LOS PARADOS sabemos que los Tacos es uno de los platillos tradicionales en la comida de las familias mexicanas, a todo mexicano le gusta tener en su mesa una tortilla caliente para iniciar los alimentos; en cualquier momento del día una tortilla es excelente alimento.

Las tortillas de maíz son especialmente importantes en la gastronomía mexicana, con ellas se preparan los tacos, tacos dorados, flautas, quesadillas, enchiladas, chilaquiles, totopos, etc. Las tortillas se comen calientes, siempre envolviendo otro alimento, como carnes, huevo, y diversas comidas.

Los tacos suelen ir acompañados de alguna salsa. El taco, como cualquier otra manifestación de cultura culinaria de México, está directamente asociado a los ingredientes utilizados en cada región geográfica del país.

TAQUERÍA LOS PARADOS es una empresa con más de 45 años de experiencia haciendo tacos a tu servicio, lo que nos permite garantizarte servicio y calidad en todos nuestros servicios.

Especialistas en tacos desde 1965
</span>
    </div>
    </div>

    <div class="slider">
    <ul class="slides">
      <li>
        <img src="../img/sliderg2.jpg"> <!--imagen del slider-->
     </li>
      <li>
       <img src="../img/sliderg1.jpg"> <!--imagen del slider--> 
     </li>
     <li>
        <img src="../img/sliderg3.jpg"> <!--imagen del slider-->
     </li>
     <li>
        <img src="../img/sliderg4.jpg"> <!--imagen del slider-->
     </li>
    </ul>
  </div>
  <div class="container">


<center>
<img src="../img/tacos.png" width=250>
<img src="../img/logo2.png" width=200>
<img src="../img/LOGO3.jpg" width=250>
<img src="../img/menu.png" width=250>
<img src="../img/LOGO4.png" width=250>
</center>

<center>
<h1>LOS REYES DEL HOGAR</h1>
</center>

  <center>  
<embed src="../img/blancanieves.pdf" width="400" height="450" href="url_pdf.pdf"></embed>
<embed src="../img/caperucitaroja.pdf" width="400" height="450" href="url_pdf.pdf"></embed>
<embed src="../img/lasirenita.pdf" width="400" height="450" href="url_pdf.pdf"></embed>
</center>

<center>
<h1>PERSONAJES</h1>
</center>

<center>
<img src="../img/blancanieves.jpg" width=300>
<img src="../img/sirenita.jpg" width=300>
<img src="../img/caperucita.jpg" width=300>
</center>
</div>
</div>
<?php
Page::footer();
?>