<?php
require("../lib/page.php");
Page::header("Reservaciones");
//se inicia el buscador de reservaciones 
if(!empty($_POST))
{
	$search = trim($_POST['buscar']);
	$sql = "SELECT * FROM reservaciones, clientes, sucursales WHERE reservaciones.codigo_cliente = clientes.codigo_cliente AND reservaciones.codigo_sucursal = sucursales.codigo_sucursal AND alias LIKE ? ORDER BY alias";
	$params = array("%$search%");
}
else
{
	$sql = "SELECT * FROM reservaciones, clientes, sucursales WHERE reservaciones.codigo_cliente = clientes.codigo_cliente AND reservaciones.codigo_sucursal = sucursales.codigo_sucursal ORDER BY alias";
	$params = null;
}
$data = Database::getRows($sql, $params);
if($data != null)
{
?>
<div class="container">
<!-- se crea el formulario -->
<form method='post'>
	<div class='row'>
		<div class='input-field col s6 m4'>
			<i class='material-icons prefix'>search</i>
			<input id='buscar' type='text' name='buscar'/>
			<label for='buscar'>Buscar</label>
		</div>
		<div class='input-field col s6 m4'>
			<button type='submit' class='btn tooltipped waves-effect green' data-tooltip='Busca por nombre'><i class='material-icons'>check_circle</i></button>
		</div>
		<div class='input-field col s12 m4'>
			<a href='save.php' class='btn waves-effect indigo'><i class='material-icons'>add_circle</i></a>
		</div>
	</div>
</form><table class='striped'>
	
	<thead>
		<tr>
			<th>CLIENTE</th>
			<th>FECHA</th>
			<th>HORA</th>
			<th>SUCURSAL</th>
			<th>ESTADO</th>
			<th>ACCIÓN</th>
		</tr>
	</thead>
	<tbody>
	<a type="button" class='btn waves-effect indigo' href = "./grafico_reservaciones.php">Grafico de reservacion por cliente</a>
<a type="button"  class='btn waves-effect indigo' href = "./grafico_Rsucursales.php">Grafico de reservacio por sucural</a>
</div>
<?php
	foreach($data as $row)
	{
		print("
			<tr>
				<td>".$row['nombres_cliente']."</td>
				<td>".$row['fecha_reservacion']."</td>
				<td>".$row['hora_reservacion']."</td>
				<td>".$row['nombre_sucursal']."</td>
				<td>
		");
		if($row['estado_reservacion'] == 1)
		{
			print("<i class='material-icons'>visibility</i>");
		}
		else
		{
			print("<i class='material-icons'>visibility_off</i>");
		}
		print("
				</td>
				<td>
					<a href='save.php?id=".$row['codigo_reservacion']."' class='blue-text'><i class='material-icons'>mode_edit</i></a>
					<a href='delete.php?id=".$row['codigo_reservacion']."' class='red-text'><i class='material-icons'>delete</i></a>
				</td>
			</tr>
		");
	}
	print("
		</tbody>
	</table>
	");
} //Fin de if que comprueba la existencia de registros.
else
{
	Page::showMessage(4, "No hay registros disponibles", "save.php");
}
Page::footer();
?>