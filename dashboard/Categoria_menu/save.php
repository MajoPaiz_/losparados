<?php
require("../lib/page.php");
//verifica el id , si no se encuentra te manda al formulario de guardar 
if(empty($_GET['id']) && ctype_digit($_GET['id']))
{
    Page::header("Agregar El tipo de menu");
    $id = null;
    $nombre = null;
    $descripcion = null;
}
else
{
    Page::header("Modificar El tipo de menu");
    $id = $_GET['id'];
    $sql = "SELECT * FROM tipo_menu WHERE codigo_tipomenu = ?";
    $params = array($id);
    $data = Database::getRow($sql, $params);
    $nombre = $data['tipo_menu'];
    $descripcion = $data['descripcion'];
}
// valida los valore que se acaban de ingresar en el formulario 
if(!empty($_POST))
{
    $_POST = Validator::validateForm($_POST);
  	$nombre = $_POST['nombre'];
  	$descripcion = $_POST['descripcion'];
    if($descripcion == "")
    {
        $descripcion = null;
    }

    try 
    {
      	if($nombre != "")
        {
            if($id == null)
            {
                $sql = "INSERT INTO tipo_menu(tipo_menu, descripcion) VALUES(?, ?)";
                $params = array($nombre, $descripcion);
            }
            else
            {
                $sql = "UPDATE tipo_menu SET tipo_menu = ?, descripcion = ? WHERE codigo_tipomenu = ?";
                $params = array($nombre, $descripcion, $id);
            }
            if(Database::executeRow($sql, $params))
            {
                Page::showMessage(1, "Operación satisfactoria", "index.php");
            }
            else
            {
                throw new Exception("Operación fallida");
            }
        }
        else
        {
            throw new Exception("Debe digitar un nombre");
        }
    }
    catch (Exception $error)
    {
        Page::showMessage(2, $error->getMessage(), null);
    }
}
?>
<div class="container">
<!-- se crea el formulario -->
<form method='post'>
    <div class='row'>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>note_add</i>
            <input id='nombre' type='text' name='nombre' class='validate' value='<?php print($nombre); ?>' required/>
            <label for='nombre'>Nombre</label>
        </div>
        <div class='input-field col s12 m6'>
            <i class='material-icons prefix'>description</i>
            <input id='descripcion' type='text' name='descripcion' class='validate' value='<?php print($descripcion); ?>'/>
            <label for='descripcion'>Descripción</label>
        </div>
    </div>
    <div class='row center-align'>
        <a href='index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
        <button type='submit' class='btn waves-effect blue'><i class='material-icons'>save</i></button>
    </div>
</form>
</div>
<?php
Page::footer();
?>