<?php
require("../lib/page.php");
Page::header("Categorías");
//if para verificar tiempo
if (!isset($_SESSION['tiempo'])) {
    $_SESSION['tiempo']=time();
}
else if (time() - $_SESSION['tiempo'] > 600) {
    session_destroy();

 Page::showMessage(3, "amigo  se tardo en entrar a la pagina otra vez ", "../main/login.php");
    die(); 
    }
//Aqui se crea un If para crear un buscador 
if(!empty($_POST))
{
	$search = trim($_POST['buscar']);
	$sql = "SELECT * FROM tipo_menu WHERE tipo_menu LIKE ? ORDER BY tipo_menu";
	$params = array("%$search%");
}
else
{
	$sql = "SELECT * FROM tipo_menu ORDER BY tipo_menu";
	$params = null;
}
$data = Database::getRows($sql, $params);
if($data != null)
{
?>
<div class="container">
<!-- aqui se crea el contenedor para la busqueda y la tabla donde se muestran los valores -->
<form method='post'>
	<div class='row'>
		<div class='input-field col s6 m4'>
			<i class='material-icons prefix'>search</i>
			<input id='buscar' type='text' name='buscar'/>
			<label for='buscar'>Buscar</label>
		</div>
		<div class='input-field col s6 m4'>
			<button type='submit' class='btn tooltipped waves-effect green' data-tooltip='Busca por nombre'><i class='material-icons'>check_circle</i></button>
		</div>
		<div class='input-field col s12 m4'>
			<a href='save.php' class='btn waves-effect indigo'><i class='material-icons'>add_circle</i></a>
		</div>
	</div>
</form>
<table class='striped'>
	<thead>
		<tr>
			<th>NOMBRE</th>
			<th>DESCRIPCIÓN</th>
			<th>ACCIÓN</th>
			<th>MENU</th>
		</tr>
	</thead>
	<tbody>
</div>
<?php
	foreach($data as $row)
	{
		print("
			<tr>
				<td>".$row['tipo_menu']."</td>
				<td>".$row['descripcion']."</td>
				<td>
					<a href='save.php?id=".$row['codigo_tipomenu']."' class='blue-text'><i class='material-icons'>edit</i></a>
					<a href='delete.php?id=".$row['codigo_tipomenu']."' class='red-text'><i class='material-icons'>delete</i></a>
					<td><a href='../reportes/reporte_tipo.php?id=".$row['codigo_tipomenu']."'>ver</a></td>
					</td>
			</tr>
		");
	}
	print("
		</tbody>
	</table>
	");
} //Fin de if que comprueba la existencia de registros.
else
{
	Page::showMessage(4, "No hay registros disponibles", "save.php");
}
Page::footer();
?>