<?php
require("../lib/page.php");
Page::header("Eliminar usuario");

 // toma el id para despues eliminarlo 
if(!empty($_GET['id']) && ctype_digit($_GET['id']))
{
    $id = $_GET['id'];
}
else
{
    header("location: index.php");
}
 // se dirige a la tabla usuarios y verifica si el id es el mismo para poder eliminar
if(!empty($_POST))
{
	$id = $_POST['id'];
	try 
	{
		if($id != $_SESSION['codigo_usuario'])
		{
			$sql = "DELETE FROM usuarios WHERE codigo_usuario = ?";
		    $params = array($id);
		    if(Database::executeRow($sql, $params))
            {
                Page::showMessage(1, "Operación satisfactoria", "index.php");
            }
            else
            {
				throw new Exception("Operación fallida");
            }
		}
		else
		{
			throw new Exception("No se puede eliminar a sí mismo");
		}
	} 
	catch (Exception $error) 
	{
		Page::showMessage(2, $error->getMessage(), "index.php");
	}
}
?>
<div class="container">
<!-- se crea el formulario -->
<form method='post'>
	<div class='row center-align'>
		<input type='hidden' name='id' value='<?php print($id); ?>'/>
		<button type='submit' class='btn waves-effect red'><i class='material-icons'>remove_circle</i></button>
		<a href='index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
	</div>
</form>
</div>
<?php
Page::footer();
?>