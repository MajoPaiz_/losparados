<?php
require("../lib/page.php");
Page::header("Menu");
//abre el buscar 
if(!empty($_POST))
{
	$search = trim($_POST['buscar']);
	$sql = "SELECT * FROM menu, tipo_menu WHERE menu.codigo_tipomenu = tipo_menu.codigo_tipomenu AND tipo_menu LIKE ? ORDER BY tipo_menu";
	$params = array("%$search%");
}
else
{
	$sql = "SELECT * FROM menu, tipo_menu WHERE menu.codigo_tipomenu = tipo_menu.codigo_tipomenu ORDER BY tipo_menu";
	$params = null;
}
$data = Database::getRows($sql, $params);
if($data != null)
{
?>
<div class="container">
<!-- crea el formulario -->
<form method='post'>
	<div class='row'>
		<div class='input-field col s6 m4'>
			<i class='material-icons prefix'>search</i>
			<input id='buscar' type='text' name='buscar'/>
			<label for='buscar'>Buscar</label>
		</div>
		<div class='input-field col s6 m4'>
			<button type='submit' class='btn tooltipped waves-effect green' data-tooltip='Busca por nombre'><i class='material-icons'>check_circle</i></button>
		</div>
		<div class='input-field col s12 m4'>
			<a href='save.php' class='btn waves-effect indigo'><i class='material-icons'>add_circle</i></a>
		</div>
	</div>
</form>
<table class='striped'>
	<thead>
		<tr>
			<th>IMAGEN</th>
			<th>NOMBRE</th>
			<th>PRECIO (US$)</th>
			<th>CATEGORÍA</th>
			<th>ESTADO</th>
			<th>ACCIÓN</th>
		</tr>
	</thead>
	<tbody>
</div>
<?php
	foreach($data as $row)
	{
		print("
			<tr>
				<td><img src='data:image/*;base64,".$row['imagen_menu']."' class='materialboxed' width='100' height='100'></td>
				<td>".$row['nombre_menu']."</td>
				<td>".$row['precio']."</td>
				<td>".$row['tipo_menu']."</td>
				<td>
		");
		if($row['estado_menu'] == 1)
		{
			print("<i class='material-icons'>visibility</i>");
		}
		else
		{
			print("<i class='material-icons'>visibility_off</i>");
		}
		print("
				</td>
				<td>
					<a href='save.php?id=".$row['codigo_menu']."' class='blue-text'><i class='material-icons'>mode_edit</i></a>
					<a href='delete.php?id=".$row['codigo_menu']."' class='red-text'><i class='material-icons'>delete</i></a>
				</td>
			</tr>
		");
	}
	print("
		</tbody>
	</table>
	");
} //Fin de if que comprueba la existencia de registros.
else
{
	Page::showMessage(4, "No hay registros disponibles", "save.php");
}
Page::footer();
?>