<?php
require("../lib/page.php");
Page::header("Clientes");?>
<div class="container">
<div class='input-field col s6 m4 center'>
			 <i class="large material-icons">group_work</i>
		</div>
<?php
//Aqui se crea un If para crear un buscador 
if (!isset($_SESSION['tiempo'])) {
    $_SESSION['tiempo']=time();
}
else if (time() - $_SESSION['tiempo'] > 600) {
    session_destroy();

 Page::showMessage(3, "amigo  se tardo en entrar a la pagina otra vez ", "../main/login.php");
    die(); 
    }
if(!empty($_POST))
{
	$search = trim($_POST['buscar']);
	$sql = "SELECT * FROM clientes WHERE apellidos_cliente LIKE ? OR nombres_cliente LIKE ? ORDER BY apellidos_cliente";
	$params = array("%$search%", "%$search%");
}
else
{
	$sql = "SELECT * FROM clientes ORDER BY apellidos_cliente";
	$params = null;
}
$data = Database::getRows($sql, $params);
if($data != null)
{
?>
<!-- aqui se crea el contenedor para la busqueda y la tabla donde se muestran los valores -->
<form method='post'>
	<div class='row'>
		<div class='input-field col s6 m4'>
			<i class='material-icons prefix'>search</i>
			<input id='buscar' type='text' name='buscar'/>
			<label for='buscar'>Buscar</label>
		</div>
		<div class='input-field col s6 m4'>
			<button type='submit' class='btn tooltipped waves-effect green' data-tooltip='Busca por nombres o apellidos'><i class='material-icons'>check_circle</i></button>
		</div>
		<div class='input-field col s12 m4'>
			<a href='save.php' class='btn waves-effect indigo'><i class='material-icons'>add_circle</i></a>
		</div>
	</div>
</form>
<!-- crea las tablas y ponerle nombre a cda campo-->
<table class='striped'>
	<thead>
		<tr>
			<th>IMAGEN</th>
			<th>ALIAS</th>
			<th>NOMBRE</th>
			<th>APELLIDOS</th>
			<th>CORREO</th>
			<th>TELEFONO</th>
			<th>RESERVACIONES</th>
			<th>EVENTOS</th>
			<th>BANEOS</th>
		</tr>
	</thead>
	<tbody>
</div>
<?php
// ordena los valore que se han omado de la base de datos 
	foreach($data as $row)
	{
		print("
			<tr>
				<td><img src='data:image/*;base64,".$row['imagen_perfil']."' class='materialboxed' width='100' height='100'></td>
				<td>".$row['alias']."</td>
				<td>".$row['nombres_cliente']."</td>
				<td>".$row['apellidos_cliente']."</td>
				<td>".$row['email_cliente']."</td>
				<td>".$row['telefono_cliente']."</td>
			    <td><a href='../reportes/reportes_reservaciones.php?id=".$row['codigo_cliente']."'>ver</a></td>
				<td><a href='../reportes/reporte_eventos.php?id=".$row['codigo_cliente']."'>ver</a></td>
				<td><a href='../reportes/reporte_baneado.php?id=".$row['codigo_cliente']."'>ver</a></td>
				
				<td>
					<a href='save.php?id=".$row['codigo_cliente']."' class='blue-text'><i class='material-icons'>mode_edit</i></a>
					<a href='delete.php?id=".$row['codigo_cliente']."' class='red-text'><i class='material-icons'>delete</i></a>
				</td>
			</tr>
		");
	}
	print("
		</tbody>
	</table>
	");
} //Fin de if que comprueba la existencia de registros.
else
{
	Page::showMessage(4, "No hay registros disponibles", "save.php");
}
Page::footer();
?>