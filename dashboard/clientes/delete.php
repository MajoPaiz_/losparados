<?php
require("../lib/page.php");
Page::header("Eliminar producto");
// toma el id seleccionado
if(!empty($_GET['codigo_tipomenu']) && ctype_digit($_GET['codigo_tipomenu']))
{
    $id = $_GET['codigo_tipomenu'];
}
else
{
    header("location: index.php");
}
// elimina el Id seleccionado 
if(!empty($_POST))
{
	$id = $_POST['id'];
	try 
	{
		$sql = "DELETE FROM clientes WHERE codigo_cliente = ?";
	    $params = array($id);
	    Database::executeRow($sql, $params);
	    header("location: index.php");
	}
	catch (Exception $error) 
	{
		Page::showMessage(2, $error->getMessage(), "index.php");
	}
}
?>
<div class="container">
<!-- se crea el formulario -->
<form method='post'>
	<div class='row center-align'>
		<input type='hidden' name='codigo_tipomenu' value='<?php print($id); ?>'/>
		<button type='submit' class='btn waves-effect red'><i class='material-icons'>remove_circle</i></button>
		<a href='index.php' class='btn waves-effect grey'><i class='material-icons'>cancel</i></a>
	</div>
</form>
</div>
<?php
Page::footer();
?>